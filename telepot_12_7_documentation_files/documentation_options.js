var DOCUMENTATION_OPTIONS = {
    URL_ROOT: '',
    VERSION: '12.7',
    LANGUAGE: 'en',
    COLLAPSE_INDEX: false,
    FILE_SUFFIX: '.html',
    HAS_SOURCE: true,
    SOURCELINK_SUFFIX: '.txt'
};