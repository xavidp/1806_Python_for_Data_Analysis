var READTHEDOCS_DATA = {
    project: "telepot",
    version: "latest",
    language: "en",
    programming_language: "py",
    subprojects: {},
    canonical_url: "https://telepot.readthedocs.io/en/latest/",
    theme: "alabaster",
    builder: "sphinx",
    docroot: "/doc/",
    source_suffix: ".rst",
    api_host: "https://readthedocs.org",
    commit: "3792fde2",

    global_analytics_code: 'UA-17997319-1',
    user_analytics_code: null
};

